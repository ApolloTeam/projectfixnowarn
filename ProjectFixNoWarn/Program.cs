﻿using System.Linq;

namespace ProjectFixNoWarn
{
	using System;
	using System.Collections.Generic;
	using System.IO;
	using System.Threading.Tasks;
	using System.Text.RegularExpressions;

	class Program
	{
		static void Main(string[] args)
		{
			if (args.Any())
			{
				try
				{
					const RegexOptions regexOptions = RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase;
					IEnumerable<string> files = Directory.EnumerateFiles(
						args.First(),
						"*.csproj",
						SearchOption.AllDirectories);
					Parallel.ForEach(files, (file) =>
					{
						Console.WriteLine($"Fixing: {Path.GetFileNameWithoutExtension(file)}");
						string fileContent = File.ReadAllText(file);
						string search;
						string replace;
						if (!Regex.IsMatch(fileContent, "<NoWarn>", regexOptions))
						{
							search = "</Project>";
							replace = "<PropertyGroup><NoWarn>$(NoWarn);NU1605;NU1603</NoWarn></PropertyGroup></Project>";
						}
						else
						{
							search = "</NoWarn>";
							replace = "$(NoWarn);NU1605;NU1603</NoWarn>";
						}

						string newfileContent = fileContent.Replace(search, replace);
						File.WriteAllText(file, newfileContent);
					});
				}
				catch (Exception exception)
				{
					Console.WriteLine(exception.Message);
				}
			}
		}
	}
}
